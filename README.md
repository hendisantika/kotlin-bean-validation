# Kotlin Bean Validation

This is the code for the Kotlin Bean Validation Tutorial.

### Run locally

```
git clone https://gitlab.com/hendisantika/kotlin-bean-validation.git
``` 

```
mvn clean spring-boot:run
```

### Screen shot

Welcome Page

![Welcome Page](img/welcome.png "Welcome page")

Add New Data

![Add New Data](img/validate.png "Validate Data")

Validate Again

![Validate Data again](img/validate2.png "Validate Data")

Successful Page

![Successful Page](img/congrats.png "Successful Page")

