package com.hendisantika.kotlinbeanvalidation

import com.hendisantika.kotlinbeanvalidation.entity.User
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import org.springframework.web.context.WebApplicationContext

@SpringBootApplication
class KotlinBeanValidationApplication

fun main(args: Array<String>) {
    runApplication<KotlinBeanValidationApplication>(*args)
}

@Service
@Scope(WebApplicationContext.SCOPE_SESSION)
class RegistrationService {

    var user = User()

}