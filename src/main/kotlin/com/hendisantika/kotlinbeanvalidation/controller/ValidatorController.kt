package com.hendisantika.kotlinbeanvalidation.controller

import com.hendisantika.kotlinbeanvalidation.RegistrationService
import com.hendisantika.kotlinbeanvalidation.entity.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.Errors
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.context.WebApplicationContext
import javax.validation.Valid

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-bean-validation
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-25
 * Time: 08:30
 */
@Controller
@RequestMapping("/")
@Scope(WebApplicationContext.SCOPE_REQUEST)
class ValidatorController(@Autowired val registrationService: RegistrationService) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model): String {
        model.addAttribute("user", User())
        return "index"
    }

    @RequestMapping(method = arrayOf(RequestMethod.POST))
    fun doPost(@Valid user: User, //The @Valid annotation tells Spring to Validate this object
               errors: Errors)  //Spring injects this class into this method. It will hold any
    //errors that are found on the object
            : String {
        val result: String
        when {
            //Test for errors
            errors.hasErrors() -> result = "index"
            else -> {
                //Otherwise proceed to the next page
                registrationService.user = user
                result = "redirect:/congrats"
            }
        }
        return result
    }
}