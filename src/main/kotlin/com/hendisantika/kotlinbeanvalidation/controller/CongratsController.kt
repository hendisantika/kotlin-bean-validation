package com.hendisantika.kotlinbeanvalidation.controller

import com.hendisantika.kotlinbeanvalidation.RegistrationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.context.WebApplicationContext

/**
 * Created by IntelliJ IDEA.
 * Project : kotlin-bean-validation
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-25
 * Time: 08:31
 */
@Controller
@RequestMapping("/congrats")
@Scope(WebApplicationContext.SCOPE_REQUEST)
class CongratsController(@Autowired val registrationService: RegistrationService) {

    @RequestMapping(method = arrayOf(RequestMethod.GET))
    fun doGet(model: Model): String {
        print("Hi request")
        model.addAttribute("welcomeMessage", "Congratulations ${registrationService.user.firstName} ${registrationService.user.lastName}")
        model.addAttribute("user", registrationService.user)
        return "/congrats"
    }
}